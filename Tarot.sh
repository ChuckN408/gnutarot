#!/bin/sh

echo "Welcome to GNU+Tarot!"
echo "---------------------"

Majormax=$(sed -n '$=' Major);
Minormax=$(sed -n '$=' Minor);

# have cards picked at line-interals of three?
RanMin=$((1 + $RANDOM % $Minormax));
RanMaj=$((1 + $RANDOM % $Majormax));

##############################Output
sed $RanMaj'q;d' Major
echo "###########################################"
sed $RanMin'q;d' Minor
